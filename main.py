#!/usr/bin/env python
import time
import pygame
import math
import numpy as np
from collections import deque
import editdistance
import logging
import random
import sys
from enum import Enum

logging.basicConfig(level=logging.DEBUG)
_LOGGER = logging.getLogger("MorseGame")
_LOGGER.disabled = True


# Morse code translation dictionary and its inverse
MORSE_LETTERS = {'A': '.-', 'B': '-...', 'C': '-.-.',
                 'D': '-..',    'E': '.',      'F': '..-.',
                 'G': '--.',    'H': '....',   'I': '..',
                 'J': '.---',   'K': '-.-',    'L': '.-..',
                 'M': '--',     'N': '-.',     'O': '---',
                 'P': '.--.',   'Q': '--.-',   'R': '.-.',
                 'S': '...',    'T': '-',      'U': '..-',
                 'V': '...-',   'W': '.--',    'X': '-..-',
                 'Y': '-.--',   'Z': '--..',
               
                 '0': '-----',  '1': '.----',  '2': '..---',
                 '3': '...--',  '4': '....-',  '5': '.....',
                 '6': '-....',  '7': '--...',  '8': '---..',
                 '9': '----.'
                 }
MORSE_SYMBOLS = {symbol: letter for letter, symbol in MORSE_LETTERS.items()}

# Game modes accessible using the CMD symbol (~3 second key press)
DEFAULT_GAME_MODE = '0'
GAME_MODES = {'D': [("WHO MADE ME", "DAN"), ("AGAIN", "DAN"), ("CORRECT", "")],
              'A': [("STATE UN IDENTITY", "0012"), ("SINK", "")],
              'N': [("SINK", "")],
              'E': [("WHAT IS YOUR UBOAT NUMBER", "U110"), ("STATE UN IDENTITY NUMBER", "0012"),
                    ("UNLOCK CODE SINK HH", "")],
              '0': [("", "")]
              }

# IO pin configuration
MORSE_KEY_PIN = 24
TX_SWITCH_PIN = 23
SPEED_KEY_PIN = 4


# Game state variable containing the messages and methods to check input
class GameState:
    # Variable defaults
    challenge = None
    response = None
    receivedBuffer = None

    # Constructor
    def __init__(self, messages):
        self.messages = messages
        self.progress()

    # Check if the user's input is correct
    def check_input(self, symbol):
        _LOGGER.debug("Appending {} and checking against target".format(symbol))
        self.receivedBuffer.append(symbol)
        return self.response != "" and editdistance.eval("".join(self.receivedBuffer), self.response) < 2

    # Progress to the next message
    def progress(self):
        _LOGGER.info("Incrementing game level")
        self.challenge, self.response = self.messages.pop(0)
        self.reset_input()
    
    # Reset the input buffer
    def reset_input(self):
        self.receivedBuffer = deque("_"*len(self.response), maxlen=len(self.response))

    # Return a string showing the game state (used by print())
    def __str__(self):
        if self.challenge and self.response:
            return "Game in progress - target messages:\n  {0}\n  {1}".format(
                    self.challenge + " -> " + self.response,
                    "\n  ".join([c + " -> " + r for c, r in self.messages])
                )
        elif self.challenge:
            return "Game has ended - final message:\n  {0}".format(
                    self.challenge + " -> " + self.response
                )
        else:
            return "No game in progress"

    # Return a string which could reproduce this object if evaluated
    def __repr__(self):
        return "GameState({0})".format([(self.challenge, self.response)] + self.messages)


LOW_SOUND_SAMPLES = [(2**15)*math.sin(2.0 * math.pi * 700 * t / 44100) for t in range(0, 22050)]
LOW_SOUND_SAMPLES = np.int16(LOW_SOUND_SAMPLES)
HIGH_SOUND_SAMPLES = [(2**15)*math.sin(2.0 * math.pi * 900 * t / 44100) for t in range(0, 22050)]
HIGH_SOUND_SAMPLES = np.int16(HIGH_SOUND_SAMPLES)


class MorseCodeTxRxMode(Enum):
    SEND = 0
    RECEIVE = 1


class BaseMorseCodeGame:
    mode = None

    def __init__(self, speeds=3, volumes=8):
        # Ensure we use a *copy* of the game mode's messages, otherwise they get removed permanently
        self.state = GameState(GAME_MODES[DEFAULT_GAME_MODE][:])

        self.volume_steps = [volume/(volumes-1) for volume in range(volumes)]
        self.current_volume = volumes

        self.speed_steps = [0.03 + 0.02*speed/(speeds-1) for speed in range(speeds)]
        self.current_speed = 0

        # Set up sound
        pygame.mixer.init(44100, -16, 1, 1024)
        self.sound_channel = pygame.mixer.Channel(5)

        # Initialise sounds
        self.sounds = {"low_morse": pygame.mixer.Sound(LOW_SOUND_SAMPLES),
                       "high_morse": pygame.mixer.Sound(HIGH_SOUND_SAMPLES),
                       "no": pygame.mixer.Sound("no.wav"),
                       "yes": pygame.mixer.Sound("yes.wav"),
                       }

        self.key_was_up = self.morse_key_pressed()
        self.set_txrx_mode()
        pygame.init()

        _LOGGER.debug("Game initialised")

    def increase_speed(self, *_):
        if self.current_speed < len(self.speed_steps)-1:
            self.current_speed += 1
        _LOGGER.debug("Game speed is %s", self.current_speed)

    def decrease_speed(self, *_):
        if self.current_speed > 0:
            self.current_speed -= 1
        _LOGGER.debug("Game speed is %s", self.current_speed)

    def increase_volume(self):
        if self.current_volume < len(self.volume_steps)-1:
            self.current_volume += 1
            for sound in self.sounds.values():
                sound.set_volume(self.volume_steps[self.current_volume])
        _LOGGER.debug("Sound volume is %s", self.current_volume)

    def decrease_volume(self):
        if self.current_volume > 0:
            self.current_volume -= 1
            for sound in self.sounds.values():
                sound.set_volume(self.volume_steps[self.current_volume])
        _LOGGER.debug("Sound volume is %s", self.current_volume)

    def set_txrx_mode(self, *_):
        if self.tx_mode_set():
            self.set_send()
        else:
            self.set_receive()

    def set_send(self):
        _LOGGER.debug("Switching to transmit mode")
        self.mode = MorseCodeTxRxMode.SEND

    def set_receive(self):
        _LOGGER.debug("Switching to receive mode")
        self.mode = MorseCodeTxRxMode.RECEIVE

    def play_morse(self, num_steps):
        self.sounds["low_morse"].play(-1)
        for _ in range(0, num_steps):
            if self.mode == MorseCodeTxRxMode.SEND:
                break

            time.sleep(self.speed_steps[self.current_speed])
        self.sounds["low_morse"].stop()

    def play_break(self, num_steps):
        for _ in range(0, num_steps):
            if self.mode == MorseCodeTxRxMode.SEND:
                return

            time.sleep(self.speed_steps[self.current_speed])

    def play_character(self, character):
        for symbol in character:
            if self.mode == MorseCodeTxRxMode.SEND:
                return

            if symbol == ".":
                self.play_morse(4)   # dit
            elif symbol == "-":
                self.play_morse(12)  # dash
            self.play_break(8)

    def play_string(self, text_string):
        for character in text_string:
            if self.mode == MorseCodeTxRxMode.SEND:
                return

            if character in MORSE_LETTERS:
                self.play_character(MORSE_LETTERS[character])
            self.play_break(16)
        self.play_break(60)

    def morse_key_pressed(self):
        raise NotImplementedError("BaseMorseCodeGame should not be instantiated directly, use a child class")

    def tx_mode_set(self):
        raise NotImplementedError("BaseMorseCodeGame should not be instantiated directly, use a child class")

    def run_main_loop(self):
        # Set up user input, send a status message to the command line
        time_since_user_acted = 0
        symbol_string = ""
        _LOGGER.debug("target string is %s", self.state.response)
        _LOGGER.info("ready")
        while True:
            # While we're in receive mode, loop the challenge message
            if self.mode == MorseCodeTxRxMode.RECEIVE:
                self.state.reset_input()
                symbol_string = ""
                time_since_user_acted = 0
                _LOGGER.debug("Prompting the user with message: %s", self.state.challenge)
                self.play_string(self.state.challenge)

            # While in send mode, collect input from the user
            else:
                # Key is down
                if self.morse_key_pressed():
                    # Register a new key press by setting the state
                    if self.key_was_up:
                        self.sounds["high_morse"].play(loops=-1)
                        self.key_was_up = False
                        time_since_user_acted = 0

                # Key is up
                else:
                    # Register a new key release by setting the state and recording the symbol
                    if not self.key_was_up:
                        # Record the symbol
                        if time_since_user_acted < 7:
                            symbol_string += "."
                        elif time_since_user_acted < 40:
                            symbol_string += "-"
                        else:
                            symbol_string = "CMD"
                            _LOGGER.debug("Command key entered, waiting for command signal")

                        # Reset user input
                        self.sounds["high_morse"].stop()
                        self.key_was_up = True
                        time_since_user_acted = 0

                    # Wait for a timeout
                    else:
                        if time_since_user_acted == 14:
                            # User has sent the command signal
                            if symbol_string.startswith("CMD"):
                                symbol_string = symbol_string[3:]

                                # Valid signal received
                                if symbol_string in MORSE_SYMBOLS and MORSE_SYMBOLS[symbol_string] in GAME_MODES:
                                    self.sounds["yes"].play()
                                    time.sleep(1.0)
                                    self.sounds["yes"].stop()
                                    # Ensure we use a *copy* of the game mode's messages
                                    self.state.messages = GAME_MODES[MORSE_SYMBOLS[symbol_string]][:]
                                    self.state.progress()

                                # Invalid signal received
                                else:
                                    self.sounds["no"].play()
                                    time.sleep(0.5)
                                    self.sounds["no"].stop()

                            # User has sent morse symbols
                            elif symbol_string in MORSE_SYMBOLS:
                                if self.state.check_input(MORSE_SYMBOLS[symbol_string]):
                                    self.state.progress()
                            symbol_string = ""

                            # Print some debug messages to the terminal
                            _LOGGER.debug("target buffer is %s", self.state.response)
                            _LOGGER.debug("received buffer is %s", "".join(self.state.receivedBuffer))

            # tick
            time.sleep(self.speed_steps[self.current_speed])
            time_since_user_acted += 1


class MorseCodeGame(BaseMorseCodeGame):
    def __init__(self, *args, **kwargs):
        import RPi.GPIO as GPIO
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(MORSE_KEY_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(TX_SWITCH_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(TX_SWITCH_PIN, GPIO.BOTH, callback=self.set_txrx_mode, bouncetime=200)
        GPIO.setup(SPEED_KEY_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.add_event_detect(SPEED_KEY_PIN, GPIO.FALLING, callback=self.increase_speed, bouncetime=400)
        GPIO.setwarnings(False)
        super().__init__(*args, **kwargs)

    def morse_key_pressed(self):
        return not GPIO.input(MORSE_KEY_PIN)

    def tx_mode_set(self):
        return not GPIO.input(TX_SWITCH_PIN)


class MorseCodeMock(BaseMorseCodeGame):
    def morse_key_pressed(self):
        return bool(random.getrandbits(1))

    def tx_mode_set(self):
        return bool(random.getrandbits(1))


# If we've been explicitly called then start the default game
if __name__ == '__main__':
    if len(sys.argv) == 1:
        _LOGGER.disabled = False
        MorseCodeGame().run_main_loop()
    else:
        MorseCodeMock().run_main_loop()
